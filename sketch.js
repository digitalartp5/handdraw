let video;
let model;
let predictions = [];

let detectionHand = 0.7// вероятность открытой ладони(если больше, то да)
let detectionFist = 0.5// вероятность закрытой ладони(если больше, то да)

let brushColor1;
let brushColor2;
let lerpAmount = 0;
let colorSpeed = 0.005; // Скорость изменения цвета

let brushSize = 40;
let f = true;
let flag = false
let spring = 0.4;
let friction = 0.45;
let v = 0.5;
let r = 0;
let vx = 0;
let vy = 0;
let splitNum = 100;
let diff = 8;

function setup() {
  //createCanvas(640, 480);
  createCanvas(windowWidth, windowHeight);
  brushColor1 = color(255, 0, 0) 
  brushColor2 = color(0, 255, 238); 

  video = createCapture(VIDEO);
  video.hide();
  video.elt.addEventListener('loadeddata', startTracking); // Добавляем обработчик события loadeddata
  startTracking();
  // Загрузка модели Handtrack.js
  handTrack.load().then(m => {
    model = m;
    handTrack.startVideo(video).then(status => {
      if (status) {
        console.log('Отслеживание рук запущено');
        // Добавьте код для определения поз
      } else {
        console.log('Не удалось запустить отслеживание рук');
      }
    });
  });
}

function startTracking() {
  if (model) {
    model.detect(video.elt).then(res => {
      predictions = res;
    
      requestAnimationFrame(startTracking);

    });
  }
}

function draw() {
 // image(video, 0, 0, width, height);
// background("white");
 //console.log('Predictions: ', predictions);
  // Отображение прямоугольников вокруг обнаруженных рук
 // for (let i = 0; i < predictions.length; i++) {
  if (predictions.length > 0){
    let vector = {}
    //console.log('Predictions: ', predictions[i].label); 
    if(predictions[0].label === "open" && predictions[0].score > detectionHand){
      console.log('Predictions: ', predictions[0].label); 
      let bbox = predictions[0].bbox;
      let x = bbox[0];
      let y = bbox[1];
      let w = bbox[2];
      let h = bbox[3];
      let centerX = x + w / 2;
      let centerY = y + h / 2;
      vector.x = width - (centerX* (width / 640))
      vector.y = centerY * (height / 480)

   
      if(flag) {
     
        lerpAmount += colorSpeed; // Можно настроить скорость изменения цвета   
        lerpAmount = lerpAmount % 1;
       const currentColor = lerpColor(brushColor1, brushColor2, lerpAmount);
        stroke(currentColor);
       
           if(!f) {
             f = true;
             xm = vector.x ;
             ym = vector.y ;
           }
           if (xm === undefined || ym === undefined)
            return;
           vx += (vector.x  - xm ) * spring;
           vy += ( vector.y  - ym ) * spring;
           vx *= friction;
           vy *= friction;
       
           v += sqrt( vx*vx + vy*vy ) - v;
           v *= 0.6;
       
           oldR = r;
           r = brushSize - v;
       
           for( let i = 0; i < splitNum; ++i ) {
             oldX = xm;
             oldY = ym;
             xm += vx / splitNum;
             ym += vy / splitNum;
             oldR += ( r - oldR ) / splitNum;
             if(oldR < 1) { oldR = 1; }
             strokeWeight( oldR+diff );  // AMEND: oldR -> oldR+diff
             line( xm, ym, oldX, oldY );
             strokeWeight( oldR );  // ADD
             line( xm+diff*1.5, ym+diff*2, oldX+diff*2, oldY+diff*2 );  // ADD
             line( xm-diff, ym-diff, oldX-diff, oldY-diff );  // ADD
           }
       
         } else if(f) {
           vx = vy = 0;
           f = false;
           flag=true;
           }
  

     // stroke(255, 0, 0); // Зеленая обводка
    //  fill(255, 0, 0); // Красный цвет заливки
   //   strokeWeight(2);
   //   circle(width - (centerX* (width / 640)),centerY * (height / 480),20)
      // noFill();
      // rect(x, y, w, h);
    }
    if(predictions[0].label === "closed" ||
      predictions[0].label === "pinch" ||
      predictions[0].label === "point"){             
      flag=false;
    }
     



  }

  
}
